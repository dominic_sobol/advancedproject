var table_row = "<tr></tr>";
var table_data = "<td class='profiles'></td>";
var given_n, given_m;
var dp;
var ans;
var i_i = 1,
    i_j = 0,
    i_k = 0;

var first_profile;
var second_profile;
var table_exists = false;

var exec_stopped = true;
var time_interval = 0;
var max_prof;

function TryParseInt(str, defaultValue) {
    var retValue = defaultValue;
    if(str !== null) {
        if(str.length > 0) {
            if (!isNaN(str)) {
                retValue = parseInt(str);
            }
        }
    }
    return retValue;
}

function set_default_when_algorithm_not_started()
{
    $("#status").text("Table has not been created");
    $("#tufts_amount_shell").css("display", "none");
}

function min(first, second)
{
    if (first < second)
        return first;
    return second;
}

function max(first, second)
{
    if (first > second)
        return first;
    return second;
}

function go_over(f_prof, s_prof)
{
    var f_el, s_el;
    var is_even_zeroes_amount= true;

    for (var cnt = 0; cnt < given_m; ++cnt)
    {
        f_el = f_prof % 2;
        s_el = s_prof % 2;

        if (f_el == s_el && f_el == 1)
            return false;
        else if (f_el == s_el && f_el == 0)
            is_even_zeroes_amount = !is_even_zeroes_amount;
        else if (!is_even_zeroes_amount)
            return false;

        f_prof /= 2;
        s_prof /= 2;
        f_prof = Math.floor(f_prof);
        s_prof = Math.floor(s_prof);
    }

    if (!is_even_zeroes_amount)
        return false;

    return true;
}

function fill_with_start_values(given_n, given_m)
{
    var length = (1 << given_m);

    $("#first_profile").text("");
    $("#second_profile").text("");

    dp = new Array(length);
    ans = new Array(given_n + 1);

    for (var i = 0; i < length; ++i)
    {
        dp[i] = new Array(length);
    }

    for (var i = 0; i < length; ++i)
    {
        for (var j = 0; j < length; ++j)
        {
            if (go_over(i, j))
                dp[i][j] = 1;
            else
                dp[i][j] = 0;
        }
    }

    for (var i = 0; i <= given_n; ++i)
    {
        ans[i] = new Array(length);

        for (var j = 0; j < length; ++j)
        {
            ans[i][j] = 0;
        }
    }

    i_i = 1;
    i_k = 0;
    i_j = 0;

    ans[0][0] = 1;
    max_prof = (1 << given_m) - 1;
}

function reflect_profiles_on_table(f_prof, s_prof)
{
    var f_el, s_el;
    var is_even_zeroes_amount = true;

    for (var cnt = 1; cnt <= given_m; ++cnt)
    {
        f_el = f_prof % 2;
        s_el = s_prof % 2;

        if (f_el == s_el && f_el == 0)
        {
            $("#table tr:nth-child(" + cnt + ") td:nth-child(" + (i_i) + ")").css("background-color", "yellow");
            is_even_zeroes_amount = !is_even_zeroes_amount;
        }
        else if (is_even_zeroes_amount)
        {
            if (f_el == s_el && f_el == 1)
            {
                $("#table tr:nth-child(" + (cnt) + ") td:nth-child(" + (i_i) + ")").css("background-color", "#f20000");
                $("#table tr:nth-child(" + (cnt) + ") td:nth-child(" + (i_i + 1) + ")").css("background-color", "#f20000");
                return false;
            }
            else if (f_el == 1 && s_el == 0)
                $("#table tr:nth-child(" + cnt + ") td:nth-child(" + (i_i) + ")").css("background-color", "brown");
            else if (f_el == 0 && s_el == 1)
            {
                $("#table tr:nth-child(" + cnt + ") td:nth-child(" + (i_i) + ")").css("background-color", "brown");
                $("#table tr:nth-child(" + cnt + ") td:nth-child(" + (i_i + 1) + ")").css("background-color", "brown");
            }
        }
        else
        {
            $("#table tr:nth-child(" + (cnt - 1) + ") td:nth-child(" + (i_i) + ")").css("background-color", "#f20000");
            return false;
        }

        f_prof /= 2;
        s_prof /= 2;
        f_prof = Math.floor(f_prof);
        s_prof = Math.floor(s_prof);
    }

    if (!is_even_zeroes_amount)
    {
        $("#table tr:nth-child(" + given_m + ") td:nth-child(" + (i_i) + ")").css("background-color", "#f20000");
        return false;
    }

    return true;
}

function get_profiles(f_prof, s_prof)
{
    var f_el, s_el;

    first_profile = "";
    second_profile = "";

    for (var i = 0; i < given_m; ++i)
    {
        f_el = f_prof % 2;
        s_el = s_prof % 2;

        if (f_el == 1)
            first_profile += '1';
        else
            first_profile += '0';

        if (s_el == 1)
            second_profile += '1';
        else
            second_profile += '0';

        f_prof /= 2;
        s_prof /= 2;
        f_prof = Math.floor(f_prof);
        s_prof = Math.floor(s_prof);
    }

    $("#first_profile").text(first_profile);
    $("#second_profile").text(second_profile);
}

function dye_profiles_in_default_color()
{
    $("#table tr td:nth-child(" + (i_i) + ")").css("background-color", "#8ab2f2");
    $("#table tr td:nth-child(" + (i_i + 1) + ")").css("background-color", "#8ab2f2");
}

function check_even_zeroes(profile)
{
    var is_even_zeroes = true;
    var res = ans[given_n - 1][profile];

    for (var i = 0; i < given_m; ++i)
    {
        if (profile % 2 == 0)
            is_even_zeroes = !is_even_zeroes;
        else if (!is_even_zeroes)
            return 0;

        profile /= 2;

        profile = Math.floor(profile);
    }

    if (!is_even_zeroes)
        return 0;

    return res;
}

function get_answer()
{
    var res = 0;

    for (var i = 0; i < dp.length; ++i)
    {
        res += check_even_zeroes(i);
    }

    return res;
}

function show_profiles(sign)
{
    if (i_i == given_n && sign == '+')
    {
        $("#tufts_amount").text(get_answer() + "");
        $("#status").text("Algorithm has finished its work");
        $("#tufts_amount_shell").css("display", "block");

        return;
    }
    else
    {
        $("#status").text("Algorithm is working");
        $("#tufts_amount_shell").css("display", "none");
    }

    if (sign == '+')
    {
        if (i_j == 0 && i_k == 0)
            $("#table tr td:nth-child(" + (i_i - 1) + ")").css("background-color", "grey");
        ans[i_i][i_j] += ans[i_i - 1][i_k] * dp[i_k][i_j];

        dye_profiles_in_default_color();

        reflect_profiles_on_table(i_k, i_j);
        get_profiles(i_k, i_j);

        if (i_k == max_prof)
        {
            i_k = 0;
            if (i_j == max_prof)
            {
                i_j = 0;
                ++i_i;

            }
            else
                ++i_j;
        }
        else
            ++i_k;

    }
    else if (sign == '-' && i_i >= 1)
    {
        if (i_i == given_n)
        {
            i_i = given_n - 1;
            i_j = max_prof;
            i_k = max_prof + 1;
        }

        if (i_j == i_k && i_j == 0 && i_i == 1)
        {
            alert("You cannot go back - you are on start");
            return;
        }
        if (i_k == 0)
        {
            i_k = max_prof;
            if (i_j == 0)
            {
                i_j = max_prof;
                --i_i;
                //$("#table tr td:nth-child(" + (i_i + 1) + ")").css("background-color", "#8ab2f2");
            }
            else
                --i_j;
        }
        else
            --i_k;


        ans[i_i][i_j] -= ans[i_i - 1][i_k] * dp[i_k][i_j];

        dye_profiles_in_default_color();

        reflect_profiles_on_table(i_k, i_j);
        get_profiles(i_k, i_j);
    }
}

function auto_exec()
{
    show_profiles('+');

    if (i_i < given_n && time_interval > 0)
    {

        setTimeout(function ()
        {
            exec_stopped = false;

            if (time_interval == -1)
            {
                exec_stopped = true;
                return;
            }

            auto_exec();
        }, time_interval);
    }
    else if (i_i >= given_n)
    {
        show_profiles('+');
        exec_stopped = true;
    }

}

$(document).ready(
function()
{
    set_default_when_algorithm_not_started();

    $("#right_arrow").click(function()
    {
       if (!exec_stopped)
           alert("You are in auto mode");
       else if (table_exists )
       {
            show_profiles('+');
       }
       else
       {
           alert("Create the table first");
       }
    });
    $("#stop_exec").click(function ()
    {
        if (!table_exists)
        {
            alert("Create the table first");
            return;
        }

        time_interval = -1;
        exec_stopped = true;
    });
    $("#auto_exec").click(function ()
    {
        if (exec_stopped && table_exists)
        {
            time_interval = TryParseInt($("#time_list option:selected").text());

            if (!isNaN(time_interval))
            {
                auto_exec();
            }
            else
                alert("Select the delay from the drop-down list");
        }
        else if (!table_exists)
        {
            alert("Create the table first");
        }
        else if (!exec_stopped)
        {
            alert("Stop current execution and reset delay you want");
        }
    })
    $("#left_arrow").click(function()
    {
        if (!table_exists)
        {
            alert("Please, create the table");
            return;
        }
        if (exec_stopped)
            show_profiles('-');
        else
            alert("Auto mode is on");
    });
    $("#left_arrow").mouseover(function () {
        this.src = "Images/left_arrow_hovered.png";
    });
    $("#left_arrow").mouseout(function () {
        this.src = "Images/left_arrow.png";
    });
    $("#right_arrow").mouseover(function () {
        this.src = "Images/right_arrow_hovered.png";
    });
    $("#right_arrow").mouseout(function () {
        this.src = "Images/right_arrow.png";
    });

    $("#create_table_button").click(
        function() {
            if (!exec_stopped)
            {
                alert("Auto mode is on");
                return;
            }
            $("#table").html("");

            given_n = TryParseInt($("#given_n").val());
            given_m = TryParseInt($("#given_m").val());

            if (given_n > 0 && given_m > 0 && given_n <= 7 &&given_m <= 7)
            {
                var tmp = given_m;
                given_m = min(given_n, given_m);
                given_n = max(given_n, tmp);

                for (var i = 0; i < given_m; ++i)
                {
                    $("#table").append(table_row);
                }
                for (var i = 0; i < given_n; ++i)
                {
                    $("#table tr").append(table_data)
                }
                table_exists = true;
                fill_with_start_values(given_n, given_m);
                time_interval = -1;

                $("#status").text("Table has been created");
                $("#tufts_amount_shell").css("display", "none");
            }
            else
            {
                $("#status").text("Table has not been created");
                alert("Input fields should contain only numbers, be filled and contain values (0, 7]");
                $("#first_profile").text("");
                $("#second_profile").text("");
                table_exists = false;
                exec_stopped = true;
            }
        }
    );
    $("#create_table_button").hover(
        function ()
        {
            if ($("#create_table_button").css("border-width") == "0px")
                $("#create_table_button").css("border", "#749de0 2px solid");
            else
                $("#create_table_button").css("border", "#749de0 0px solid");
        }
    )
    $(".user_input").on('input', function ()
    {
        if (this.value.length > 1)
            this.value = this.value.slice(0, 1);
    });
});